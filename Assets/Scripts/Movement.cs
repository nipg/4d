﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	// order: XZ, YZ, XW, movement
	private Vector4 delta;

	private void Start () {
		delta = new Vector3();
		Screen.lockCursor = true;
	}
	
	private void Update () {
		delta = Vector4.zero;
		delta.x = Input.GetAxis("Mouse X");
		delta.y = Input.GetAxis("Mouse Y");

		if (Input.GetKey(KeyCode.H)) {
			delta.x += -0.1f;
		}
		if (Input.GetKey(KeyCode.K)) {
			delta.x += 0.1f;
		}
		
		if (Input.GetKey(KeyCode.U)) {
			delta.y += 0.1f;
		}
		if (Input.GetKey(KeyCode.J)) {
			delta.y += -0.1f;
		}

		if (Input.GetKey(KeyCode.A)) {
			delta.z = -0.1f;
		}
		if (Input.GetKey(KeyCode.D)) {
			delta.z = 0.1f;
		}

		if (Input.GetKey(KeyCode.W)) {
			delta.w = 0.5f;
		}
		if (Input.GetKey(KeyCode.S)) {
			delta.w = -0.5f;
		}

		delta = delta / 5;
	}

	public Vector4 GetDelta() {
		return delta;
	}

}
