﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MainScript : MonoBehaviour {

	private Movement movement;
	private List<GameObject> gameObjects;

	private void Start() {
		NativeInterface.resetCamera();
		movement = GetComponent<Movement>();
		gameObjects = new List<GameObject>();
	}

	private void Update() {
		CleanUp();

		Vector4 delta = movement.GetDelta();
		NativeInterface.moveCamera(-delta[1], 0f, delta[0], delta[2], 0f, 0f, delta[3], 0f, 0f, 0f);

		float camX = NativeInterface.cameraPos(0);
		float camY = NativeInterface.cameraPos(1);
		float camZ = NativeInterface.cameraPos(2);
		Camera.main.transform.position = new Vector3(camX, camY, camZ);
		float camDirX = NativeInterface.cameraDirection(0);
		float camDirY = NativeInterface.cameraDirection(1);
		float camDirZ = NativeInterface.cameraDirection(2);
		float camUpX = NativeInterface.cameraUp(0);
		float camUpY = NativeInterface.cameraUp(1);
		float camUpZ = NativeInterface.cameraUp(2);
		Camera.main.transform.LookAt(new Vector3(camX+camDirX, camY+camDirY, camZ+camDirZ), 
		                             new Vector3(camUpX, camUpY, camUpZ));

		List<List<List<Vector3>>> objectDescriptors = new List<List<List<Vector3>>>();
		NativeInterface.resetIterators();
		while (NativeInterface.hasObject()) {
			List<List<Vector3>> obj = new List<List<Vector3>>();
			while (NativeInterface.hasFacet()) {
				List<Vector3> facet = new List<Vector3>();
				while (NativeInterface.hasVertex()) {
					float x = NativeInterface.vertexCoord(0);
					float y = NativeInterface.vertexCoord(1);
					float z = NativeInterface.vertexCoord(2);
					facet.Add(new Vector3(x, y, z));
					NativeInterface.nextVertex();
				}
				obj.Add(facet);
				NativeInterface.nextFacet();
			}
			objectDescriptors.Add(obj);
			NativeInterface.nextObject();
		}

		List<Mesh> meshes = MeshGenerator.Get(objectDescriptors);
		gameObjects = MeshGenerator.DisplayMeshes(meshes);
	}

	private void CleanUp() {
		foreach (GameObject go in gameObjects) {
			Destroy(go.GetComponent<MeshFilter>().sharedMesh);
			Destroy(go.GetComponent<MeshRenderer>().renderer.sharedMaterial);
			Destroy(go);
		}
	}
}
