﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class MeshGenerator {

	private static readonly List<Color> colors = new List<Color> { Color.red, Color.green, Color.blue, Color.yellow, Color.gray };

	public static List<Mesh> Get(List<List<List<Vector3>>> objectDescriptors) {
		List<Mesh> meshes = new List<Mesh>();
		for (int obj = 0; obj < objectDescriptors.Count; ++obj ) {
			for (int facet = 0; facet < objectDescriptors[obj].Count; ++facet ) {
				Mesh mesh = new Mesh();
				List<int> triangles = new List<int>();
				List<Vector3> vertices = new List<Vector3>();
				List<Vector2> uv = new List<Vector2>();
				for (int vertex = 0; vertex < objectDescriptors[obj][facet].Count; ++vertex) {
					uv.Add(Vector2.zero);
					vertices.Add(objectDescriptors[obj][facet][vertex]);
				}

				for (int triangle = 0; triangle < vertices.Count - 2; ++triangle) {
					triangles.Add(0);
					triangles.Add(triangle + 1);
					triangles.Add(triangle + 2);
					triangles.Add(triangle + 2);
					triangles.Add(triangle + 1);
					triangles.Add(0);
				}

				mesh.vertices = vertices.ToArray();
				mesh.uv = uv.ToArray();
				mesh.SetTriangles(triangles.ToArray(), 0);
				mesh.RecalculateBounds();
				mesh.RecalculateNormals();
				meshes.Add(mesh);
			}

		}

		return meshes;
	}

	public static List<GameObject> DisplayMeshes(List<Mesh> meshes) {
		List<GameObject> objects = new List<GameObject>();
		for (int i = 0; i < meshes.Count; ++i) {
			Mesh mesh = meshes[i];
			GameObject gameObj = GameObject.Instantiate(Resources.Load<GameObject>("Object")) as GameObject;
			gameObj.name = "Object" + i;
			gameObj.transform.parent = GameObject.Find("Objects").transform;
			gameObj.GetComponent<MeshFilter>().mesh = mesh;
			Material material = gameObj.GetComponent<MeshRenderer>().renderer.material;
			float alpha = material.color.a;
			Color targetColor = colors[i % colors.Count];
			material.color = new Color(targetColor.r, targetColor.g, targetColor.b, alpha);
			objects.Add(gameObj);
		}
		return objects;
	}

	public static void Test() {
		List<List<List<Vector3>>> vertices = new List<List<List<Vector3>>>();
		vertices.Add(new List<List<Vector3>>());
		vertices[0].Add(new List<Vector3>{
			new Vector3(0.0f, 0.0f, 1.0f),
			new Vector3(0.0f, 0.5f, 1.0f),
			new Vector3(0.5f, 0.5f, 1.0f),
			new Vector3(0.5f, 0.0f, 1.0f)		
		});
		vertices[0].Add(new List<Vector3>{
			new Vector3(-0.3f, -0.3f, 1.0f),
			new Vector3(-0.3f, 0.0f, 1.0f),
			new Vector3(0.0f, -0.3f, 1.0f)
		});
		List<Mesh> meshes = MeshGenerator.Get(vertices);
		DisplayMeshes(meshes);
	}

}
